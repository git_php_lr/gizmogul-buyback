<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->id();
            $table->string('shipment_no');
            $table->string('uuid');
            $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('location_id')->nullable()->references('id')->on('locations');
            $table->foreignId('address_id')->nullable()->references('id')->on('user_addresses');
            $table->foreignId('payout_method_id')->nullable()->references('id')->on('user_payout_methods');
            $table->decimal('tax', 10, 2)->nullable();
            $table->decimal('total', 10, 2);
            $table->decimal('sub_total', 10, 2);
            $table->enum('shipment_type', ['1', '2'])->default('1');
            $table->string('shipping_type')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('shipping_carrier')->nullable();
            $table->string('tracking_no')->nullable();
            $table->string('tracking_url')->nullable();
            $table->text('qrcode')->nullable();
            $table->text('barcode')->nullable();
            $table->text('label_url')->nullable();
            $table->enum('status', ['pending', 'approved', 'shipped', 'delivered', 'received', 'completed',  're-packaged', 'please-ship', 'cancel'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
