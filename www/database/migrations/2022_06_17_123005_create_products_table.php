<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('category_id')->nullable()->references('id')->on('categories');
            $table->foreignId('brand_id')->references('id')->on('brands');
            $table->foreignId('attribute_family_id')->references('id')->on('attribute_families');
            $table->foreignId('parent_id')->nullable()->references('id')->on('products')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->string('sku');
            $table->decimal('price', 8, 2)->nullable();
            $table->enum('type',['simple','configurable'])->default('configurable');
            $table->string('attribute_family')->nullable();
            $table->longText('description')->nullable();
            $table->longText('photos')->nullable();
            $table->enum('status', ['0', '1'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
