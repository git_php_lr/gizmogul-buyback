<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('country_id')->references('id')->on('countries');
            $table->string('name');
            $table->string('email');
            $table->string('address');
            $table->string('apartment')->nullable();
            $table->string('phone');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->enum('type', ['shipping', 'billing'])->default('shipping');
            $table->enum('status', [0, 1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}
