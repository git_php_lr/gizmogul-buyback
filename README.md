Download
--------
VirtualBox - https://www.virtualbox.org/

Vagrant - https://www.vagrantup.com/downloads

Ansible - https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

Clone and run VM
----------------
$ git clone https://github.com/devel-inc/gizmogul-buyback.git repo

$ cd repo/tools

$ vagrant up

Connect to VM
-------------
$ vagrant ssh

Setup Host and Preview
----------------------
Add a line to /etc/hosts: 

192.168.100.45  gizmogul-buyback.dev

To preview, go to the browser:
https://gizmogul-buyback.dev/

Other VM Notes
--------------
To display SSH identity file location and SSH settings, use:

$ vagrant ssh-config
